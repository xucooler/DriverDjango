from . import views
from django.urls import path,include

urlpatterns = [
    path(r'', views.login),
    path(r'index', views.index),
    path(r'add', views.add),
    path(r'addinfo', views.addinfo),
    path(r'change', views.change),
    path(r'delete', views.delete),
    path(r'logout', views.logout),
    path(r'userinfo',views.userinfo),
    path(r'surplus',views.surplus),
    path(r'detail',views.detail),
    path(r'order',views.order),
    path(r'history',views.history),
    path(r'test',views.test),
    path(r'test2',views.test2),
    # 查询未完成的学车记录
    path(r'unfinish',views.unfinish),
    # 查询已完成的学车记录
    path(r'finish',views.finish),
    # 学员签到
    path(r'signin',views.signin),
    # 查询是否有该学生信息
    path(r'check',views.check),
    # 学生端录入信息
    path(r'stuinfoadd',views.stuinfoadd),
    # 设置约考状态
    path(r'special',views.special),
    # 取消练车
    path(r'remove',views.remove),
    # 查看是否约考
    # path(r'istest',views.istest),
    # 教练端
    path(r'infointeacher',views.infointeacher),
    # 跳转到审核界面
    path(r'admincheck',views.admincheck),
    # 通过审核
    path(r'pass',views.checkpass),
    # 跳转到未签到学员界面
    path(r'unsign',views.unsign),
    # 查询是否为约考状态
    path(r'checkspecial',views.checkspecial),
]