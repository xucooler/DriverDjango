# Generated by Django 2.1.7 on 2019-10-13 06:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0003_auto_20191013_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='all_appoinment',
            name='date',
            field=models.DateField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='appoinment',
            name='date',
            field=models.DateField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='date',
            field=models.DateField(auto_now_add=True),
        ),
    ]
