from .models import all_appoinment,appoinment,record,stu_info
from datetime import date, timedelta

def test():
    print('*'*30)

def all_appoinment_job():
  tomorrow = date.today() + timedelta(days=+1)
  all_appo = all_appoinment(date=tomorrow)
  all_appo.save()

def appoinment_job():
    tomorrow_one = appoinment(date=date.today() + timedelta(days=+1), section_id=1, all_order=0, carOneSurplus=0,
                              carTwoSurplus=0)
    tomorrow_two = appoinment(date=date.today() + timedelta(days=+1), section_id=2, all_order=0, carOneSurplus=0,
                              carTwoSurplus=0)
    tomorrow_three = appoinment(date=date.today() + timedelta(days=+1), section_id=3, all_order=0, carOneSurplus=0,
                                carTwoSurplus=0)
    tomorrow_four = appoinment(date=date.today() + timedelta(days=+1), section_id=4, all_order=0, carOneSurplus=0,
                               carTwoSurplus=0)
    tomorrow_five = appoinment(date=date.today() + timedelta(days=+1), section_id=5, all_order=0, carOneSurplus=0,
                               carTwoSurplus=0)
    tomorrow_six = appoinment(date=date.today() + timedelta(days=+1), section_id=6, all_order=0, carOneSurplus=0,
                              carTwoSurplus=0)
    tomorrow_seven = appoinment(date=date.today() + timedelta(days=+1), section_id=7, all_order=0, carOneSurplus=0,
                                carTwoSurplus=0)
    tomorrow_eight = appoinment(date=date.today() + timedelta(days=+1), section_id=7, all_order=0, carOneSurplus=0,
                                carTwoSurplus=0)
    tomorrow_one.save()
    tomorrow_two.save()
    tomorrow_three.save()
    tomorrow_four.save()
    tomorrow_five.save()
    tomorrow_six.save()
    tomorrow_seven.save()
    tomorrow_eight.save()

# 把昨天的未完成记录全部设置为已完成
# 检测学员的约考状态
def change_status():
    items = record.objects.filter(date=date.today() + timedelta(days=-1),is_finish=0)
    for item in items:
        item.is_finish = 1
        item.save()
    stus = stu_info.objects.filter(is_test=True)
    for stu in stus:
        if stu.testtime == date.today():
            stu.is_test = False
            stu.save()
