from django.db import models
from datetime import date, timedelta

# Create your models here.
# 用户信息表
class stu_info(models.Model):
    # 姓名
    name = models.CharField(max_length=5)
    # 年龄
    age = models.IntegerField()
    # 微信号
    weixin = models.CharField(max_length=50, unique=True)
    # 手机号
    phone_num = models.CharField(max_length=11)
    # 进度代号
    # process_status = models.IntegerField()
    # 是否约考
    is_test = models.BooleanField(default=False)
    # 驾照
    # driver_license = models.CharField(max_length=10, default="")
    # 住址
    place = models.CharField(max_length=250, default="")
    # 身份证
    idcard = models.CharField(max_length=20, default='')
    # 身份情况
    status = models.IntegerField(default=2)
    # 临考日期
    testtime = models.DateField(default="2099-11-11")


# 时间段表
class time_table(models.Model):
    # 时间段
    section = models.CharField(max_length=50, verbose_name="时间段")
    # 对应id
    section_id = models.IntegerField(verbose_name="对应id")

    def __str__(self):
        return self.section
    class Meta:
        # 末尾不加s
        verbose_name_plural = '时间段表'

# 教练表
class teacher(models.Model):
    # 教练名
    name = models.CharField(max_length=5, verbose_name="教练名")
    # 性别
    sex = models.BooleanField(verbose_name="性别(打钩为男，不打为女)")
    # 年龄
    age = models.IntegerField(verbose_name="年龄")
    # 微信号
    weixin = models.CharField(max_length=50, verbose_name="微信号")
    # 手机号
    phone = models.CharField(max_length=11, verbose_name="手机号")
    # 对应的车的车牌
    hiscar = models.OneToOneField('carTable', on_delete='CASCADE', default='', verbose_name="车牌号")
    def __str__(self):
        return self.name
    class Meta:
        # 末尾不加s
        verbose_name_plural = '教练表'

# 车和车牌号
class carTable(models.Model):
    # 车位号
    carID = models.IntegerField(verbose_name="车位号")
    # 车牌号
    license_num = models.CharField(max_length=20, verbose_name="车牌号")
    # 时间段
    time_ID = models.IntegerField(default=1)
    def __str__(self):
        return self.license_num
    class Meta:
        # 末尾不加s
        verbose_name_plural = '车和车牌号'

# 约车记录表
class record(models.Model):
    # 学车人
    # student = models.CharField(max_length=5)
    # 微信号
    weixin = models.CharField(max_length=50)
    # 时间id
    section_id = models.IntegerField()
    # 日期
    date = models.DateField()
    # 教练
    # teacher = models.CharField(max_length=5)
    # 车牌号
    car_number = models.CharField(max_length=50)
    # 是否完成
    is_finish = models.BooleanField(default=False)
    # 是否取消
    is_cancel = models.BooleanField(default=False)
    # 是否是临考学员
    is_test = models.BooleanField(default=False)


# 预约总表：
class all_appoinment(models.Model):
    # 日期
    date = models.DateField()
    # 时间段id
    # section_id = models.IntegerField()
    # 预约人数
    order_number = models.IntegerField(default=0)

# 实时预约表：
class appoinment(models.Model):
    # 总车位
    # all = models.IntegerField()
    # 日期
    date = models.DateField()
    # 时间id
    section_id = models.IntegerField()
    # 总预约人数
    all_order = models.IntegerField()
    # 车位1已预约人数
    carOneSurplus = models.IntegerField()
    # 车位2已预约人数
    carTwoSurplus = models.IntegerField()
