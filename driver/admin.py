from django.contrib import admin
from .models import time_table,teacher,carTable
# Register your models here.
admin.site.site_title="腾飞驾校后台管理"
admin.site.site_header="腾飞驾校后台管理"
admin.site.index_title="腾飞驾校后台管理"

# Underwriter admin model
class teacherAdmin(admin.ModelAdmin):
    # 需要显示的字段信息
    list_display = ('name', 'sex', 'age', 'weixin', 'phone', 'hiscar')

    # 设置哪些字段可以点击进入编辑界面，默认是第一个字段
    list_display_links = ('name', 'sex', 'age', 'weixin', 'phone', 'hiscar')

class time_tableAdmin(admin.ModelAdmin):
    # 需要显示的字段信息
    list_display = ('section', 'section_id')

    # 设置哪些字段可以点击进入编辑界面，默认是第一个字段
    list_display_links = ('section', 'section_id')

class carTableAdmin(admin.ModelAdmin):
    # 需要显示的字段信息
    list_display = ('license_num', 'carID', 'time_ID')

    # 设置哪些字段可以点击进入编辑界面，默认是第一个字段
    list_display_links = ('license_num', 'carID', 'time_ID')

# 注册时，在第二个参数写上 admin model
admin.site.register(time_table, time_tableAdmin)
admin.site.register(teacher,teacherAdmin)
admin.site.register(carTable,carTableAdmin)
